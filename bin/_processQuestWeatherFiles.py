import logging
import bisect
import datetime
import numpy as np
import os
import sys
import pdb
import TSD
from pylab import *
import scipy.interpolate as interpolate

FILE_LOGGING_LEVEL = logging.DEBUG
SCREEN_LOGGING_LEVEL = logging.DEBUG

weather_dir = "../QuestWeather/"

def dateRange(dt_1, dt_2, delt):
    rg = []

    while dt_1 < dt_2:
        rg.append(dt_1)
        dt_1 += delt
    return np.array(rg, copy = False)

def configureLogger():
    logger = logging.getLogger('weather_proc')
    logger.setLevel(logging.DEBUG)

    # create file handler and a screen handler
    fh = logging.FileHandler('weather_proc.log')
    fh.setLevel(FILE_LOGGING_LEVEL)
    ch = logging.StreamHandler()
    ch.setLevel(SCREEN_LOGGING_LEVEL)
    # create formatter and add it to the handlers

    #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    
    return 

def getLogger():
    return logging.getLogger('weather_proc')

def loadTemperatureDataForStation(station):
    station_temp_ts_fn = os.path.join(weather_dir, "%s.TS.npy" % station)
    station_temp_vs_fn = os.path.join(weather_dir, "%s.VS.npy" % station)

    if not os.path.isfile(station_temp_ts_fn):
        getLogger().critical("Temperature file for station %s does not exist (%s)." % (station, station_temp_fn))

    if not os.path.isfile(station_temp_vs_fn):
        getLogger().critical("Temperature file for station %s does not exist (%s)." % (station, station_temp_fn))        
        
    return np.load(station_temp_ts_fn), np.load(station_temp_vs_fn)

def convertTemperatureDataForStation(station):
    logger = getLogger()

    logger.info("Processing station %s" % station)
    station_temp_fn = os.path.join(weather_dir, "quest_weather.%s.temperature.csv" % station)
    if not os.path.isfile(station_temp_fn):
        logger.critical("Temperature file for station %s does not exist (%s)." % (station, station_temp_fn))

    with open(station_temp_fn) as temp_file:
        lines = temp_file.readlines()
        logger.debug("Loaded file.")

        ts_str = map(lambda ln: ln[:16], lines)
        vs_str = map(lambda ln: ln[17:], lines)
        logger.debug("Parsed file lines.")

        fmt = "%m/%d/%Y %H:%M"
        ts = map(lambda dt_str: datetime.datetime.strptime(dt_str, fmt), ts_str)
        logger.debug("Parsed time array")
        
        vs = map(float, vs_str)
        logger.debug("Parsed value array")

        logger.info("Parsed station file %s" % station_temp_fn)

    ts = np.array(ts, copy = False)
    vs = np.array(vs, copy = False)

    np.save(os.path.join(weather_dir, "%s.TS.npy" % station), ts)
    np.save(os.path.join(weather_dir, "%s.VS.npy" % station), vs)
    logger.debug("Saved to numpy arrays.")
    logger.info("Finished converting station %s." % station)
    return 


def main():
    logger = getLogger()
    conn = TSD.connect()
    cur = conn.cursor()

    temp_files = map(lambda fn: os.path.join(weather_dir, fn), \
                     filter(lambda fn: fn.endswith("temperature.csv"), os.listdir(weather_dir)))

    cmd_fn = os.path.join(weather_dir, 'quest_weather_downloading_commands_with_humidity.txt')
    cmd_res_fn = os.path.join(weather_dir, 'quest_commands_processing_with_humidity.txt')

    # Get the zips in the cmd_file
    zip_code_list = map(lambda x: x.strip(), set(filter(lambda x: len(x.strip()) == 5, open(cmd_fn).readlines())))
    zip_code_list.sort()
    num_zips = len(set(zip_code_list))

    def line_parser(ln):
        comp = ln.split()
        return (comp[4], comp[7])

    cmd_res_file = open(cmd_res_fn)
    res_contents = cmd_res_file.readlines()
    zip_lines = filter(lambda ln: ln.startswith("Station"), res_contents)
    zip_map = map(line_parser, zip_lines)
            
    # except IOError, xcpt:
    #     logger.critical("Error, filename %s not found." % cmd_res_fn)
    #     sys.exit(1)

    zip_station_map = {}

    for zip_code, station in zip_map:
        zip_station_map.setdefault(zip_code,[]).append(station)

    # for zip_code in zip_code_list:
    #     print "%s:\t%d" % (zip_code, len(zip_station_map[zip_code]))

    # counts = dict([(x,0) for x in range(6)])
    # for zip_code in zip_code_list:
    #     counts[len(zip_station_map[zip_code])] += 1

    # print "Counts:"
    # for x in counts:
    #     print "%d: %d" % (x, counts[x])

    for low_count in [0,1,2,3]:
        low_zips = [z for z in zip_code_list if len(zip_station_map[z]) == low_count]
        for zp in low_zips:
            logger.warning("Low Station Coverage: Zip %s has %d stations - %s." % \
                           (zp, low_count, ",".join(zip_station_map[zp])))

    if len(filter(lambda x: x.endswith("npy"), os.listdir(weather_dir))) == 0:
        logger.info("Could not find npy files in direcotry %s.  Rebuilding" % weather_dir)
        for zip_code in zip_code_list:
            zip_stations = zip_station_map[zip_code]
            map(convertTemperatureDataForStation, zip_stations)

    makeDay = lambda dt: datetime.date(month = dt.month, day = dt.day, year = dt.year)

    for zip_ndx, zip_code in enumerate(zip_code_list):

        if cur.connection.open:
            cur.connection.stat()
        else:
            conn = TSD.connect()
            cur = conn.cursor()

        zip_stations = zip_station_map[zip_code]

        if len(zip_stations) == 1: continue
        temp_data = map(loadTemperatureDataForStation, zip_stations)

        for station, ts_vs in zip(zip_stations, temp_data):

            stat_id = TSD.addWeatherSiteToDatabase(cur, \
                                                       point_id = station, \
                                                       source_description = "Wunderground Station", \
                                                       zip_code = zip_code)

            logger.info("Added Wunderground Site %s with ID %s to database" % (station, stat_id))

            ts, vs = ts_vs
            used_normalization, corrected_times = TSD.makeMonotonic(ts)
            if not used_normalization: 
                timezone = "LOCAL"
            else:
                timezone = "NORMALIZED"
            TSD.addTemperatureDataToDatabase(cur, stat_id, corrected_times, vs, timezone, "F")
            logger.info("Added Data for Site %s to database" % station)


        # We're going to plot 5 days of data every 3 months.
        day_to_plot = makeDay(temp_data[0][0][ 30 ])

        # print "Plotting for station %s in zip code %s" % (",".join(zip_stations), zip_code)


        min_min_day = min(map(lambda ts_vs: ts_vs[0][0], temp_data))
        max_min_day = max(map(lambda ts_vs: ts_vs[0][0], temp_data))

        min_max_day = min(map(lambda ts_vs: ts_vs[0][-1], temp_data))
        max_max_day = max(map(lambda ts_vs: ts_vs[0][-1], temp_data))

        if (max_min_day - min_min_day).days > 30:
            logger.warning("Data for zip code loses %d days at the beginning. Min_Min_Day = %s, max_min_day=%s" % ((max_min_day - min_min_day).days, \
                                                                                                                       "%.2d//%.2d//%.4d" % (min_min_day.month, min_min_day.day, min_min_day.year), \
                                                                                                                       "%.2d//%.2d//%.4d" % (max_min_day.month, max_min_day.day, max_min_day.year)))

        if (max_max_day - min_max_day).days > 30:
            logger.warning("Data for zip code loses %d days at the beginning. Min_max_day = %s, max_max_day=%s" % ((max_max_day - min_max_day).days, \
                                                                                                                       "%.2d-%.2d-%.4d" % (min_max_day.month, min_max_day.day, min_max_day.year), \
                                                                                                                       "%.2d-%.2d-%.4d" % (max_max_day.month, max_max_day.day, max_max_day.year)))


        _secFromEpoch = np.frompyfunc(lambda dt: (dt - datetime.datetime(1970,1,1)).total_seconds(),1,1)
        def secFromEpoch(arr):
            return np.array(list(_secFromEpoch(arr)))

        _dtFromSec = np.frompyfunc(lambda sec: datetime.datetime(1970,1,1) + datetime.timedelta(seconds = sec), 1, 1)
        def dtFromSec(arr):
            return np.array(list(_dtFromSec(arr)))


        def roundTime(dt):
            mn = dt.minute

            if mn == 0:
                return dt
            elif mn <= 15:
                return datetime.datetime(month = dt.month, day = dt.day, year = dt.year, hour = dt.hour, second = 0,
                                         minute = 15)
            elif mn < 30:
                return datetime.datetime(month = dt.month, day = dt.day, year = dt.year, hour = dt.hour, second = 0,
                                         minute = 30)
            elif mn < 45:
                return datetime.datetime(month = dt.month, day = dt.day, year = dt.year, hour = dt.hour, second = 0,
                                         minute = 45)
            else:
                return datetime.datetime(month = dt.month, day = dt.day, year = dt.year, hour = dt.hour, second = 0,
                                         minute = 0) + datetime.timedelta(seconds = 60 * 60)

        interpolating_functions = map(lambda ts_vs: interpolate.interp1d(secFromEpoch(ts_vs[0]), ts_vs[1]), temp_data)
        start_time = _secFromEpoch(roundTime(max_min_day))
        
        interp_ts = np.arange(start_time, _secFromEpoch(min_max_day), 15. * 60)
        interp_vs = np.array(map(lambda func: func(interp_ts), interpolating_functions))

        result_vs = np.median(interp_vs, axis = 0)

        interp_ts_dt = dtFromSec(interp_ts)

        nowstr = datetime.datetime.now().strftime("%m%d%Y%H%M")
        zip_stations.sort()
        statstr="_".join(zip_stations)
        namestr = "%s_Calculated_%s_Median_%s" % (zip_code, nowstr, statstr)

        stat_id = TSD.addWeatherSiteToDatabase(cur, \
                                                   point_id = namestr, \
                                                   source_description = "Calculated", \
                                                   zip_code = zip_code)
        logger.info("Added calculated site %s to database" % namestr)

        used_normalization, corrected_times = TSD.makeMonotonic(interp_ts_dt)
        if not used_normalization: 
            timezone = "LOCAL"
        else:
            timezone = "NORMALIZED"
        TSD.addTemperatureDataToDatabase(cur, stat_id, corrected_times, result_vs, timezone, "F")

        logger.info("Added site %s data to database" % namestr)

        
    return 
        
        









        # pdb.set_trace()

        # while day_to_plot < max_day:
        #     plot_series = []

        #     for ndx in range(len(temp_data)):
        #         # Pull out the relevant bit of the dataset.
        #         plot_start_day = day_to_plot
        #         plot_end_day = day_to_plot + datetime.timedelta(days = 8)

        #         start_ndx = getStartOfDayIndex(plot_start_day, temp_data[ndx][0])
        #         end_ndx = getStartOfDayIndex(plot_end_day, temp_data[ndx][0])
                
        #         plot_series.append((temp_data[ndx][0][start_ndx:end_ndx], \
        #                             temp_data[ndx][1][start_ndx:end_ndx]))

        #     # Create the new thing.

        #     _secFromEpoch = np.frompyfunc(lambda dt: (dt - datetime.datetime(1970,1,1)).total_seconds(),1,1)
        #     def secFromEpoch(arr):
        #         return np.array(list(_secFromEpoch(arr)))

        #     interpolating_functions = map(lambda ts_vs: interpolate.interp1d(secFromEpoch(ts_vs[0]), ts_vs[1]), temp_data)
        #     start_time = max(map(lambda ts_vs: ts_vs[0][0], filter(lambda ts_vs: len(ts_vs[0]), plot_series)))
        #     end_time = min(map(lambda ts_vs: ts_vs[0][-1], filter(lambda ts_vs: len(ts_vs[0]), plot_series)))


        #     if start_time.minute == 0:
        #         mn = 0
        #     elif start_time.minute < 15:
        #         mn = 15
        #     elif start_time.minute < 30:
        #         mn = 30
        #     elif start_time.minute < 45:
        #         mn = 45

        #     start_time = datetime.datetime(day = start_time.day, month = start_time.month, \
        #                                    year = start_time.year, \
        #                                    hour = start_time.hour, minute = mn)

        #     dt_range = dateRange(start_time, end_time, datetime.timedelta(seconds = 60 * 15))
        #     rg = secFromEpoch(dt_range)

        #     temp_interp = map(lambda func: func(rg), interpolating_functions)
        #     temp_med = np.median(temp_interp,axis=0)
            
        #     # Plot the temperature.
        #     for ts, vs in plot_series:
        #         plot(ts, vs)
        #     else:
        #         plot(dt_range, temp_med, "k-", linewidth=2)
                
        #         show()
        #         clf()

        #     # Advance the seasons.
        #     day_to_plot += datetime.timedelta(days = 91)
#    return


def getStartOfDayIndex(day, ts):
    dt = datetime.datetime(day = day.day, month = day.month, year = day.year, hour = 0, minute = 0)
    day_ndx = bisect.bisect_left(ts, dt)
    return day_ndx


if __name__ == '__main__':
    configureLogger()
    logger = getLogger()

#    try:
    main()      
    # except Exception, xcpt:
    #     logger.critical("Unknown Exception, Quitting.")
    #     logger.critical(str(xcpt))
