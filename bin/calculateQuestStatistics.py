"""This script prints out all the different statistics associated with
each of the Quest sites."""

import cProfile
import TSD
import datetime
import numpy as np
import scipy.interpolate as interp
import scipy.integrate as integrate
import pdb

def getLoadDataExtremes(cur, site_id):
    query_1 = "Select time, original_time from trend_load where site_id=%s ORDER BY time limit 1;" % site_id
    query_2 = "Select time, original_time from trend_load where site_id=%s ORDER BY time DESC limit 1;" % site_id

    cur.execute(query_1)
    first_time = cur.fetchone()[1]

    cur.execute(query_2)
    last_time = cur.fetchone()[1]

    return first_time, last_time

def getDatesOfRetrofits(cur, site_id):
    cur.execute("SELECT install_date FROM pge_retrofits where site_id=%s" % site_id)
    dates = map(lambda x: x[0], cur.fetchall())
    dates = filter(lambda x: x != None, dates)
    dates = list(set(dates))
    dates.sort()
    return dates

def printSiteHeader(cur, site_id):
    cur.execute("select point_id from sites where id=%s" % site_id)
    point_id = cur.fetchone()[0]

    cur.execute("select quest_id from pge_metadata where site_id=%s" % site_id)
    quest_id = cur.fetchone()[0]

    printHeader("Report for Site %d" % site_id)
    print "SA ID: %s" % point_id
    print "QuEST ID: %s" % quest_id

    return 

def printSiteMetadata(cur, site_id):
    query = "select id, point_id, customer_name, data_source, end_use, square_footage, address_1, address_2, city, state, zip_code, extra_info, extra_info_2 from sites where id=%s" % site_id
    cur.execute(query)

    id_site, point_id, customer_name, data_source, end_use, square_footage, \
             address_1, address_2, city, state, zip_code, extra_info, extra_info_2 = cur.fetchone()

    printHeader("Core Metadata")
    print "Database ID: %s" % id_site
    print "Point Name: %s" % point_id
    print "Customer Name: %s" % customer_name
    print "Data Source: %s" % data_source
    print "End Use: %s" % end_use
    print "Square Footage: %s" % square_footage
    print "Address 1: %s" % address_1
    print "Address 2: %s" % address_2
    print "City: %s" % city
    print "State: %s" % state
    print "Zip: %s" % zip_code
    print "Extra 1: %s" % extra_info
    print "Extra 2: %s" % extra_info_2
    

    cur.execute("SELECT site_id, acct_id, quest_id, prem_id, per_id, mailing_addr, naics2_code, comb_size, comb_size_cd, bldg_type_code, description, meter_type, climate_zone_code, rs_cd FROM pge_metadata where site_id=%s" % id_site)

    site_id, acct_id, quest_id, prem_id, per_id, mailing_addr, \
             naics2_code, comb_size, comb_size_cd, bldg_type_code, description, \
             meter_type, climate_zone_code, rs_cd = cur.fetchone()

    printHeader("PGE Metadata")
    print "Site ID: %s" % site_id
    print "Acct ID: %s" % acct_id
    print "Quest ID: %s" % quest_id
    print "Prem ID: %s" % prem_id
    print "Per ID: %s" % per_id
    print "Mailing Addr: %s" % mailing_addr
    print "NAICS2 Code: %s" % naics2_code
    print "Comb Size: %s" % comb_size
    print "Comb Size CD: %s" % comb_size_cd
    print "Bldg Type Code: %s" % bldg_type_code
    print "Description: %s" % description
    print "Meter Type: %s" % meter_type
    print "Climate Zone: %s" % climate_zone_code
    print "RS Cd: %s" % rs_cd

    return

def dtAreSameDay(dt1, dt2):
    return (dt1.month == dt2.month and dt1.day == dt2.day and dt1.year == dt2.year)

def days(ts, vs):
    if not len(ts): return []
    
    ts_cpy = list(ts[::-1])
    vs_cpy = list(vs[::-1])

    broken_ts = [[ts_cpy.pop()]]
    broken_vs = [[vs_cpy.pop()]]

    while len(ts_cpy):
        if dtAreSameDay(ts_cpy[-1], broken_ts[-1][0]):
            broken_ts[-1].append(ts_cpy.pop())
            broken_vs[-1].append(vs_cpy.pop())
        else:
            broken_ts.append([ts_cpy.pop()])
            broken_vs.append([vs_cpy.pop()])

    return broken_ts, broken_vs

def generateStatisticsForTimeSeries(ts, vs):
    """Generates day parameters for the building represented in these
    time series."""
    mean_val = np.mean(vs)
    median_val = np.median(vs)

    # pdb.set_trace()
    ts_days, vs_days = days(ts, vs)

    # These are in seconds 
    day_params = [calculateDayParams(ts, vs) for ts, vs in zip(ts_days, vs_days)]

    def makeSelector(x):
        return lambda y: y[x]

    start_times = map(makeSelector(0), day_params)
    begin_highs = map(makeSelector(1), day_params)
    end_highs = map(makeSelector(2), day_params)
    end_times = map(makeSelector(3), day_params)
    baselines = map(makeSelector(4), day_params)
    peaklines = map(makeSelector(5), day_params)
    near_base_loads = map(makeSelector(6), day_params)
    near_peak_loads = map(makeSelector(7), day_params)

    start_time = np.median(start_times)
    begin_high = np.median(begin_highs)
    end_high = np.median(end_highs)
    end_time = np.median(end_times)
    baseline = np.median(baselines)
    peakline = np.median(peaklines)
    near_base_load = np.median(near_base_loads)
    near_peak_load = np.median(near_peak_loads)
    
    return start_time, begin_high, end_high, end_time, baseline, peakline, \
           near_base_load, near_peak_load


def calculateDayParams(ts, vs):
    # First we need to translate the ts array into seconds from the start of the day.

    t_dy = ts[0]
    start_of_day = datetime.datetime(hour = 0, minute = 0, \
                                     month = t_dy.month, day = t_dy.day, year = t_dy.year)

    convertToSeconds = np.frompyfunc(lambda x: (x - start_of_day).total_seconds(), 1, 1)
    ts_sec = list(convertToSeconds(ts))

    assert(ts_sec[-1] <= 86400)

    near_base_load = np.percentile(vs, 2.5)
    near_peak_load = np.percentile(vs, 97.5)
    near_base_load = np.percentile(vs, 10)
    near_peak_load = np.percentile(vs, 90)
    median_load = np.median(vs)

    try:
        assert(vs[0] < median_load)
        assert(vs[-1] < median_load)
    except:
        return 0.0, 0.0, 86400, 86400.0, near_base_load, near_base_load, near_peak_load, near_peak_load


    # Find the first time in the day the power hits the peak.
    for ndx in range(len(vs)):
        if vs[ndx] >= near_peak_load:
            peak_start_time = ts_sec[ndx]
            break
    else:
        pdb.set_trace()
        raise Exception("Should not get here.")

    # Now find the time before that where the power was at or below the 2.5 percentile.
    for ndx in range(ndx, -1, -1):
        if vs[ndx] <= near_base_load:
            rampup_start_time = ts_sec[ndx + 1]
            break
    else:
        # pdb.set_trace()
        # raise Exception("Should not get here.")
        rampup_start_time = 0.0

    # Find the last time in the day we were at or above the peak load.
    for ndx in range(len(vs)-1, -1, -1):
        if vs[ndx] >= near_peak_load:
            peak_end_time = ts_sec[ndx]
            break
    else:
        pdb.set_trace()
        raise Exception("Should not get here.")

    # Find the time after that where the power is below the base.
    for ndx in range(ndx, len(vs)):
        if vs[ndx] <= near_base_load:
            rampdown_end_time = ts_sec[ndx]
            break
    else:
        # pdb.set_trace()
        #raise Exception("Shouldn't get here.")
        rampdown_end_time = 86400.

    if ts_sec[0] != 0:
        ts_sec.insert(0, 0)
        vs.insert(0, vs[0])

    if ts_sec[-1] != 86400:
        ts_sec.append(86400)
        vs.append(vs[-1])

    ts_sec = np.array(ts_sec, copy = False)
        

    load = interp.interp1d(ts_sec, vs)

    assert(rampup_start_time <= peak_start_time <= peak_end_time <= rampdown_end_time)

    # try:
    #     baseline = 1.0 / (rampup_start_time + (86400. - rampdown_end_time)) * \
    #                ( integrate.quad(load, 0, rampup_start_time)[0] + integrate.quad(load, rampdown_end_time, 86400.)[0])
    # except ZeroDivisionError:
    #     baseline = near_base_load

    # try:
    #     peakline = integrate.quad(load, peak_start_time, peak_end_time)[0] / (peak_end_time - peak_start_time)
    # except ZeroDivisionError:
    #     peakline = near_peak_load

    baseline, peakline = 0,0

    return rampup_start_time, peak_start_time, peak_end_time, rampdown_end_time, \
                          near_base_load, near_peak_load, baseline, peakline


# def calculateDayParams(ts, vs):
#     return 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0


def getAllLoadData(cur, site_id):
    cur.execute("SELECT original_time, value from trend_load where site_id=%d" % site_id)
    results = cur.fetchall()

    t1 = map(lambda x: x[0], results)
    t2 = map(lambda x: x[1], results)

    ts = np.array(t1, copy = False)
    vs = np.array(t2, copy = False)
    
    return ts, vs

def getLoadDataBetween(cur, site_id, lt_date, gt_date):

    lt_date_lower = lt_date - datetime.timedelta(days = 1)
    gt_date_greater = gt_date + datetime.timedelta(days = 1)

    mysql_fmt = "%Y-%m-%d %H:%M"
    lt_str = lt_date_lower.strftime(mysql_fmt)
    gt_str = gt_date_greater.strftime(mysql_fmt)

    query = "SELECT original_time, value from trend_load where site_id=%d and time>='%s' and time<='%s'" % (site_id, lt_str, gt_str)
    cur.execute(query)
    results = cur.fetchall()

    ts = np.array(map(lambda x: x[0], results), copy = False)
    vs = np.array(map(lambda x: x[1], results), copy = False)

    date_filt = np.logical_and( ts >= lt_date, ts < gt_date)

    ts = np.compress(date_filt, ts)
    vs = np.compress(date_filt, vs)    

    return ts, vs

def printSiteLoadStatistics(cur, site_id):
    printHeader("Whole Time Series Statistics")
    ts, vs = getAllLoadData(cur, site_id)
    printStatisticsForTimeSeries(ts, vs)
    return

def printSiteLoadStatisticsForIntervals(cur, site_id, dataset):

    start_date = datetime.datetime(day = 1, month = 1, year = 1901, hour = 0, minute = 0)
    end_date = datetime.datetime(day = 1, month = 1, year = 2100, hour = 0, minute = 0)

    retro_dt = map(lambda d: datetime.datetime(month = d.month, day = d.day, year = d.year, \
                                               hour = 0, minute = 0), dataset)
    retro_dt.insert(0, start_date)
    retro_dt.append(end_date)
    mysql_fmt = "%Y-%m-%d %H:%M"    

    for ndx in xrange(len(retro_dt) - 1):
        ts, vs = getLoadDataBetween(cur, site_id, retro_dt[ndx], retro_dt[ndx+1])
        printHeader("Statistics for interval %d: (%s - %s)" % (ndx + 1, \
                                                               retro_dt[ndx].strftime(mysql_fmt), \
                                                               retro_dt[ndx+1].strftime(mysql_fmt)))
        printStatisticsForTimeSeries(ts, vs)

    return

def printStatisticsForTimeSeries(ts, vs):
    mean_val = np.mean(vs)
    median_val = np.median(vs)

    start_time, begin_high, end_high, end_time, baseline, peakline, \
                near_base_load, near_peak_load = generateStatisticsForTimeSeries(ts, vs)

    print "Mean Val: %s" % mean_val
    print "Median Val: %s" % median_val

    vs_var = np.var(vs)
    
    print "Var: %s" % vs_var
    print "Std: %s" % vs_var**.5
    print "Start Time: %s" % formatTime(start_time)
    print "Begin High: %s" % formatTime(begin_high)
    print "End High: %s" % formatTime(end_high)
    print "End Time: %s" % formatTime(end_time)
    print "Baseline: %s" % baseline
    print "Near Base: %s" % near_base_load
    print "Peakline: %s" % peakline
    print "Near Peak: %s" % near_peak_load
    
    return 
    
def formatTime(sec):

    res = datetime.datetime(day = 1, month = 1, year = 2012, \
                            hour = 0, minute = 0) + datetime.timedelta(seconds = sec)

    return "%.2d:%.2d" % (res.hour, res.minute)

def printHeader(string):
    margin = 5
    print 
    print string
    print "=" * (len(string) + margin)
    return

def printIntervalInformation(site_name, start_tm, end_tm, ret_dates):
    printHeader("Interval Info")
    print "Site data goes from %s to %s" % (start_tm, end_tm)

    ret_dates = map(lambda d: datetime.datetime(month = d.month, day = d.day, year = d.year, \
                                               hour = 0, minute = 0), ret_dates)

    ret_dates.append(start_tm)
    ret_dates.append(end_tm)
    ret_dates.sort()

    mysql_fmt = "%Y-%m-%d %H:%M"
    ret_date_str = map(lambda x: x.strftime(mysql_fmt), ret_dates)

    for ndx in xrange(len(ret_dates) - 1):
        days = (ret_dates[ndx+1] - ret_dates[ndx]).total_seconds() / (24 * 60 * 60)
        print "--> Interval %d: %s - %s (%.1f)" % (ndx + 1, ret_date_str[ndx], ret_date_str[ndx+1], days)
    
    return 

def printAllSiteInformation(cur, site_id):
    # Print out the metadata
    # We want to print the range of all the data
    # The total length
    # Times of each of the retrofits and lengths of each of the periods.

    # Then we want to print out each of the statistics for each of the periods.
    # Then we want to print out the statistics for the whole time.

    # I should do this in the most API-heavy way possible, as an exercise.

    printSiteHeader(cur, site_id)

    min_date, max_date = getLoadDataExtremes(cur, site_id)
    dateset = getDatesOfRetrofits(cur, site_id)
    printIntervalInformation(site_id, min_date, max_date, dateset)

    printSiteMetadata(cur, site_id)
    printSiteRetrofitInformation(cur, site_id)

    # printSiteLoadStatistics(cur, site_id)
    # printSiteLoadStatisticsForIntervals(cur, site_id, dateset)

    return

def printSiteRetrofitInformation(cur, site_id):

    num = cur.execute("SELECT install_date, measure_code, measure_description, pge_prog_name, pge_prog_subcat, channel, delivery_type, delivery_type_desc, rs_cd FROM pge_retrofits where site_id=%s ORDER BY install_date" % site_id)
    
    retrofits = cur.fetchall()
    # retrofits.sort(key = lambda ret: ret[0])

    have_null_install_dates = True in map(lambda r: r[0] == None, retrofits)

    print "Retrofits for site %s (%d)" % (site_id, num)
    print "=" * 48    

    if have_null_install_dates:
        print "WARNING! Site has Retrofits with null install dates!"

    for ndx, ret in enumerate(retrofits):
        install_date, measure_code, measure_description, pge_prog_name, \
                      pge_prog_subcat, channel, delivery_type, delivery_type_desc, rs_cd = \
                      ret

        print "--> Retrofit %d/%d" % (ndx + 1, len(retrofits))
        print "\tInstall Date: %s" % install_date
        print "\tMeasure Code: %s" % measure_code
        print "\tMeasure Description: %s" % measure_description
        print "\tProgram Name: %s" % pge_prog_name
        print "\tProgram Subcat: %s" % pge_prog_subcat
        print "\tChannel: %s" % channel
        print "\tDelivery Type: %s" % delivery_type
        print "\tDelivery Type Desc: %s" % delivery_type_desc
        print "\tRS CD: %s" % rs_cd

    return


def main():

    conn = TSD.connect()
    cur = conn.cursor()

    num_sites = cur.execute('SELECT id, point_id FROM sites WHERE data_source="QuEST/PGE project."')

    print "There are %d sites." % num_sites

    ndx = 0
    for site_id, point_id in cur.fetchall():
        ndx +=1
        if ndx == 10:
            break

        if "." in point_id:
            continue
        printAllSiteInformation(cur, site_id)
    
    return

if __name__ == '__main__':
    # cProfile.run("main(), stats_profile")
    main()
