CREATE TABLE files (id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
       filename VARCHAR(100) NOT NULL,
       directory VARCHAR(100), 
       data_source VARCHAR(1024),
       file_contents LONGBLOB);

CREATE TABLE sites (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
       point_id VARCHAR(100), 
       customer_name VARCHAR(100), 
       data_source VARCHAR(100),
       end_use VARCHAR(100),
       square_footage VARCHAR(100),
       address_1 VARCHAR(100),
       address_2 VARCHAR(100),
       city VARCHAR(100),
       state VARCHAR(2),
       zip_code VARCHAR(7),
       extra_info VARCHAR(500),
       extra_info_2 VARCHAR(500),
       UNIQUE INDEX(point_id));

-- This table describes for each site the files that make make it up.
CREATE TABLE file_sources(
       id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
       site_id INTEGER NOT NULL, 
       file_id INTEGER NOT NULL, 
       INDEX(site_id, file_id),
       FOREIGN KEY (site_id) REFERENCES sites (id), 
       FOREIGN KEY (file_id) REFERENCES files (id));

-- I messed up when I created this.  I should change it when I get a
-- chance to id, site_id, subpoint_id and had that be the key.  But I
-- didn't, and if/when it comes up it should be an 3 hour long job to
-- create a new table and copy this one over.
CREATE TABLE trend_load (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
       site_id INTEGER NOT NULL,
       time DATETIME NOT NULL, 
       offset_sec FLOAT NOT NULL,
       original_time DATETIME NOT NULL,
       value FLOAT, 
       is_nan enum("True", "False"),
       timezone enum('LOCAL', 'UTC', 'NORMALIZED') NOT NULL, 
       unit VARCHAR(20) NOT NULL,
       UNIQUE INDEX(site_id, time),
       INDEX(site_id, original_time),
       FOREIGN KEY (site_id) REFERENCES sites (id));

-- This describes any source of weather data.
CREATE TABLE weather_stations ( id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
       point_id VARCHAR(100),  -- This should be the station name
       station_type VARCHAR(100), -- This should be like METAR, Wunderground, NOAA.
       source_description VARCHAR(255),
       address_1 VARCHAR(100),
       address_2 VARCHAR(100),
       city VARCHAR(100),
       state VARCHAR(100),
       zip_code VARCHAR(100));

-- This table describes how the sites and weather_sites match up.  
CREATE TABLE sites_to_stations (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
       site_id INTEGER NOT NULL, 
       station_id INTEGER NOT NULL, 
       is_official_source enum("True", "False"),
       distance FLOAT,
       description VARCHAR(100),
       FOREIGN KEY (site_id) REFERENCES sites (id),
       FOREIGN KEY (station_id) REFERENCES weather_stations (id));

CREATE TABLE trend_temp (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
       station_id INTEGER NOT NULL,
       time DATETIME NOT NULL, 
       offset_sec FLOAT NOT NULL,
       original_time DATETIME NOT NULL, 
       value FLOAT NOT NULL,
       is_nan enum("True", "False"),
       timezone enum('LOCAL', 'UTC', 'NORMALIZED') NOT NULL, 
       unit VARCHAR(20) NOT NULL,
       UNIQUE INDEX(weather_id, time),
       INDEX(weather_id, original_time),
       FOREIGN KEY (weather_id) REFERENCES weather_data (id));

CREATE TABLE trend_humidity (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, 
       station_id INTEGER NOT NULL,
       time DATETIME NOT NULL, 
       offset_sec FLOAT NOT NULL,
       original_time DATETIME NOT NULL, 
       value FLOAT NOT NULL,
       is_nan enum("True", "False"),
       timezone enum('LOCAL', 'UTC', 'NORMALIZED') NOT NULL, 
       unit VARCHAR(20) NOT NULL,
       UNIQUE INDEX(weather_id, time),
       INDEX(weather_id, original_time),
       FOREIGN KEY (weather_id) REFERENCES weather_data (id));

-- CREATE TABLE trend_raw (id INTEGER NOT NULL PRIMARY KEY, 
--        time_series blob NOT NULL, 
--        value_series blob, NOT NULL);

-- CREATE TABLE trend_load_raw (id INTEGER NOT NULL PRIMARY KEY,
--        timezone ENUM('LOCAL', 'UTC') NOT NULL, 
--        time_series MEDIUMBLOB NOT NULL, 
--        value_series MEDIUMBLOB NOT NULL,
--        unit VARCHAR(20),
--        FOREIGN KEY (id) REFERENCES sites (id));

-- CREATE TABLE trend_temp_raw (id INTEGER NOT NULL PRIMARY KEY,
--        timezone ENUM('LOCAL', 'UTC') NOT NULL, 
--        time_series MEDIUMBLOB NOT NULL, 
--        value_series MEDIUMBLOB NOT NULL, 
--        unit VARCHAR(20),
--        FOREIGN KEY (id) REFERENCES sites (id));

