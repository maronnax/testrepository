import TSD
import datetime
import pdb


def main():
    conn = TSD.connect()
    cur = conn.cursor()
    pdb.set_trace()
    num = cur.execute("select id from sites")
    sites = map(lambda x: x[0], cur.fetchall())
    sites.sort()

    for site in sites:
        cur.execute("select count(*) from pge_retrofits where site_id=%d" % site)
        num = cur.fetchone()[0]

        if num:
            cur.execute("select min(install_date) from pge_retrofits where site_id=%d" % site)
            retro_date = cur.fetchone()[0]
        else:
            retro_date = False


        cur.execute("select min(time), max(time) from trend_load where site_id=%d" % site)
        min_date, max_date = cur.fetchone()

        total_data = (max_date - min_date).total_seconds() / 60 / 60 / 24

        print "Site %d" % site
        print "Total Data: %.1f years" % (total_data / 365.)
        
        if retro_date:
            retro_date = datetime.datetime(month = retro_date.month, day = retro_date.day, year = retro_date.year, hour = 0, minute = 0)
            early_data = (retro_date - min_date).total_seconds() / 60 / 60 / 24
            print "Data before 1st retro: %.1f years" % (early_data / 365.)
        else:
            print "RETROFITS MISSING DATES."

        print

        
        
        
        
        

    return 


if __name__ == '__main__':
    main()
