import TSD
import pdb

def main():
    conn = TSD.connect()
    cur = conn.cursor()

    cur2 = conn.cursor()

    zip_codes = {}


    cur.execute("select id, zip_code from sites")

    for site_id, zip_code in cur.fetchall():
        cur.execute("select min(time) from trend_load where site_id=%d" % site_id)
        early_time = cur.fetchone()[0]

        cur.execute("select max(time) from trend_load where site_id=%d" % site_id)
        max_time = cur.fetchone()[0]

        if zip_code not in zip_codes:
            zip_codes[zip_code] = [early_time, max_time]

        if early_time < zip_codes[zip_code][0]:
            zip_codes[zip_code][0] = early_time

        if max_time > zip_codes[zip_code][1]:
            zip_codes[zip_code][1] = max_time

    

    fmt = "%m/%d/%Y %H:%M"
    for zc in zip_codes.keys():
        print "%s: %s - %s" % (zc, zip_codes[zc][0].strftime(fmt), zip_codes[zc][1].strftime(fmt))
    
if __name__ == '__main__':
    main()
                
