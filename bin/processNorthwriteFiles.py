import io
import MySQLdb as db
import os
import sys
import csv
import datetime
import numpy as np
import time
import pdb

def addNewFileMappingToDatabase(conn, cur, db_site_id, file_id):
    cur.execute("INSERT INTO file_sources (site_id, file_id) VALUES (%s, %s)", (db_site_id, file_id))

def addFileToDatabase(conn, cur, dat_file):
    directory, filename = os.path.split(dat_file)
    data_source = "Phil provided it for the new Jessica Project; He originally got it from..."
    file_contents = open(dat_file).read()

    file_info = (filename, directory, data_source, file_contents)

    cur.execute("INSERT INTO files (filename, directory, data_source, file_contents) VALUES (%s, %s, %s, %s)", file_info)
    cur.connection.commit()

    cur.execute("show table status like 'files'")
    db_id = cur.fetchone()[10] - 1

    return db_id

def addNewSiteToDatabase(cur, site_id, location, end_use, ts, vs, temp, vs_data_type, temp_data_type, ts_datetime_utc, ts_datetime_local):
    try:
        city, state = map(lambda x: x.strip(), location.split(","))
    except ValueError:
        city, state = location.strip(), ""


    # ADD THE NEW SITE
    new_site = (site_id, "Northwrite", end_use, city, state)
    cur.execute("INSERT INTO sites (point_id, data_source, end_use, city, state) VALUES (%s, %s, %s, %s, %s)", new_site)
    cur.connection.commit()
    cur.execute("show table status like 'sites'")
    db_id = cur.fetchone()[10] - 1


    # DO SOME PREPROCESSING WITH TIMESTAMPS
    fmt = "%Y-%m-%d %H:%M:00"

    ts_datetime_utc = np.array( ts_datetime_utc, copy = False)
    ts_datetime_local = np.array( ts_datetime_local, copy = False)

    ts_dt_str_utc = map( lambda x: datetime.datetime.strftime(x, fmt), ts_datetime_utc)
    ts_dt_str_local = map( lambda x: datetime.datetime.strftime(x, fmt), ts_datetime_local)

    offsets = (ts_datetime_local - ts_datetime_utc)
    second_offsets = map( lambda x: x.total_seconds(), offsets)


    # ADD THE BUILDING DATA
    vs_isnan = np.isnan(vs)
    new_val_table_entries = map( lambda x: (db_id, x[0], x[1], x[2], vs_data_type, x[3], x[4], "UTC"), zip(ts_dt_str_utc, vs, second_offsets, vs_isnan, ts_dt_str_local))
    cur.executemany("insert into trend_load (site_id, time, value, offset_sec, unit, is_nan, original_time, timezone) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", new_val_table_entries)
    cur.connection.commit()


    # ADD THE NEW RECORD OF THE WEATHER STREAM
    cur.execute("INSERT INTO weather_data (official_stream, data_source) VALUES (%s, %s)", (True, "Included with original data set"))
    cur.connection.commit()
    cur.execute("show table status like 'weather_data'")
    weather_stream_id = cur.fetchone()[10] - 1


    # ADD THE TEMP DATA 
    temp_isnan = np.isnan(temp)
    new_temp_table_entries = map(lambda x: (weather_stream_id, x[0], x[1], x[2], temp_data_type, x[3], x[4], "UTC"), zip(ts_dt_str_utc, temp, second_offsets, temp_isnan, ts_dt_str_local))
    cur.executemany("insert into trend_temp (weather_id, time, value, offset_sec, unit, is_nan, original_time, timezone) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", new_temp_table_entries)
    cur.connection.commit()

    # ADD A RECORD SAYING THAT THE SITE CAN BE USED WITH THE WEATHER.
    cur.execute("INSERT INTO sites_weather (site_id, weather_id) VALUES (%s, %s)", (db_id, weather_stream_id))
    
    return db_id

def convertValueSeriesTokW(data_type, ts, vs):
    print "\tConverting from %s" % data_type

    spacing = (ts[1] - ts[0])
    assert(spacing == 900.0)

    if data_type == "kWh":
        factor = 4.0
    elif data_type == "kBtu":
        factor == .293071 * 4.0
    else:
        raise Exception("Bad data_type (%s) provided" % data_type)

    return factor * vs

def convertDateTimeToSecondsFromEpoch(d_tm):
    """Takes a Python datetime object and returns seconds from the
 epoch.
    
    Arguments:
    - `d_tm`: a Python datetime object
    """

    tt_arr = [x for x in d_tm.timetuple()]
    tt_arr[-1] = 0
    return time.mktime(tt_arr)

def convertSecondsFromEpochToDateTime(sec):
    """Takes an integer representing seconds from the epoch and
 returns a python datetime object.
    
    Arguments:
    - `sec`: Seconds since the epoch
    """

    return datetime.datetime.fromtimestamp(sec)

# Testing this out.  Does this significantly show things down? 
convertDateTimeToSecondsFromEpoch = np.frompyfunc(convertDateTimeToSecondsFromEpoch, 1, 1)
convertSecondsFromEpochToDateTime = np.frompyfunc(convertSecondsFromEpochToDateTime, 1, 1)

def convertTimeString(time_str):
    time_fmt = "%m/%d/%y %H:%M"
    return datetime.datetime.strptime(time_str, time_fmt)

conn = db.connect("localhost", "maronnax", "f112358S", "building_db")
cur = conn.cursor()

data_dir = "NorthwriteData"
files = map(lambda x: os.path.join(data_dir, x), os.listdir(data_dir))

pdb.set_trace()
for dat_file in files:

    # This adds the physical file to the database.  
    file_id = addFileToDatabase(conn, cur, dat_file)

    base_name = os.path.split(dat_file)[1]
    site_id = base_name.split(".")[0].strip()

    print "Parsing %s" % dat_file

    with open(dat_file, 'Ur') as in_file:
        file_reader = csv.reader(in_file, delimiter=",")

        header_line = file_reader.next()
        data_type = header_line[2]
        temp_type = header_line[3]
        local_time = header_line[4]
        location = header_line[6]
        bldg_type = header_line[7]

        try:
            assert(data_type in ["kWh", "kW", "kBtu"])
            assert(temp_type in ["Temp [F]"])
            assert(local_time in ["LocalTime"])
            assert(location in ["Location"])
            assert(bldg_type in ["Bldg Type"])
        except AssertionError:
            print "%s has unexpected format. Skipping" % dat_file
            continue


        site_location = False
        end_use = False

        ts_str_utc = []
        ts_str_local = []

        vs = []
        temp = []

        for ndx, components in enumerate(file_reader):
            if ndx == 0: site_location = components[6]; end_use = components[7]

            ts_str_utc.append( components[1] )
            ts_str_local.append( components[4] )
            vs.append( components[2] )
            temp.append( components[3])

        else:
            ts_datetime_utc = map(convertTimeString, ts_str_utc)
            ts_datetime_local = map(convertTimeString, ts_str_local)

            vs = map(float, vs)
            temp = map(float, temp)

            ts_sec = map(convertDateTimeToSecondsFromEpoch, ts_datetime_local)

            ts_sec = np.array(ts_sec, copy = False)
            vs = np.array(vs, copy = False)
            temp = np.array(temp, copy = False)

        db_site_id = addNewSiteToDatabase(cur, site_id, site_location, end_use, ts_sec, vs, temp, data_type, "F", ts_datetime_utc, ts_datetime_local)
        addNewFileMappingToDatabase(conn, cur, db_site_id, file_id)

            

            
            




                


    # # STORE THE RAW NUMPY ARRAY FOR THE WHOLE THING
    # memfile_ts = io.BytesIO()
    # memfile_vs = io.BytesIO()
    # memfile_temp = io.BytesIO()

    # np.save(memfile_ts, ts)
    # np.save(memfile_vs, vs)
    # np.save(memfile_temp, temp)

    # memfile_ts.seek(0)
    # memfile_vs.seek(0)
    # memfile_temp.seek(0)

    # kw_data = (db_id, "LOCAL", vs_data_type, memfile_ts.getvalue(), memfile_vs.getvalue())
    # temp_data = (db_id, "LOCAL", temp_data_type, memfile_ts.getvalue(), memfile_temp.getvalue())

    # cur.execute("insert into trend_load_raw (id, timezone, unit, time_series, value_series) values (%s, %s, %s, %s, %s)", kw_data)
    # cur.execute("insert into trend_temp_raw (id, timezone, unit, time_series, value_series) values (%s, %s, %s, %s, %s)", temp_data)
    # cur.connection.commit()

    # STORE THE TIME SERIES IN THE TREND TABLES.
    # pdb.set_trace()
