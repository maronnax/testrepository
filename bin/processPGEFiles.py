import io
import MySQLdb as db
import os
import sys
import copy
import csv
import datetime
import numpy as np
import time
import pdb
import TSD


def main():

    data_dir = "PGEData"
    conn = TSD.connect()
    cur = conn.cursor()

    chunks = []
    
    for fn in map(lambda fn: os.path.join(data_dir, fn), os.listdir(data_dir)):
    #for fn in ["PGEData/ACWD.PGE.2006.csv"]:

        dat = addFileToDatabase(cur, fn)
        # chunks.extend(map(lambda x: (x[0], x[1], x[2], x[3], x[4], fn), dat))

    for x in chunks:
        print "|%15s|%35s|%15s|%25s|%75s|%25s|" % x

def parsePGEFile(fn):
    print "Processing %s" % fn
    with open(fn, "Ur") as in_file:
        file_reader = csv.reader(in_file, delimiter=",")

        line1 = file_reader.next()
        line2 = file_reader.next()
        line3 = file_reader.next()
        line4 = file_reader.next()
        line5 = file_reader.next()
        line6 = file_reader.next()
        line7 = file_reader.next()
        line8 = file_reader.next()
        line9 = file_reader.next()
        line10 = file_reader.next()
        line11 = file_reader.next()
        line12 = file_reader.next()

        assert(line8[0].lower().strip() == "customer")
        customer = line8[1].strip()
        
        # pdb.set_trace()
        sites = filter(lambda t: t, map(lambda t: t.strip(), line12[1:]))

        
        skip_sites = ["MARTINEZ   4231553040   AGGREGATE ACCOUNTS for COUNTY OF CONTRA COSTA (kW)", \
                          "OAKLAND   9803846005   TOTALIZED for KAISER (kW)", \
                          "PLEASANTON   9981329478   AGGREGATED ACCOUNTS for KOHL'S DEPT STORE (kW)", \
                          "SAN FRANCISCO   5418439005   TOTALIZED ACCOUNTS for BANK OF AMERICA (kW)", \
                          "SUNNYVALE   1272589317   AGGREGATED for NETWORK APPLIANCE (kW)", \
                          "MILPITAS   5595490679   AGGREGATED ACCOUNTS for SOLECTRON CORPORATION (kW)", \
                          "DUBLIN   8439268005   TOTALIZE METER 9284R7 9286R7 JN625 JN626 (kW)"]

        sites = filter(lambda x: x not in skip_sites, sites)

        site_data = map(chunkSite, sites)
        site_data = map(lambda x: (x[0][0], x[0][1], x[0][2], x[0][3], x[1]), zip(site_data, sites))


        data = {}
        for ndx, site in enumerate(sites):
            data[ndx + 1] = []


        time_str = []
        
        for components in file_reader:
            if len(components) == 0: 
                break

            while len(components) < len(sites) + 1:
                components.append('')

            time_str.append(components[0])

            for key in data:
                data[key].append(components[key])

        for key in data.keys():
            data[key] = map(mapToFloat, data[key])


    try:
        time_fmt = "%m/%d/%Y %I:%M %p"
        datetime.datetime.strptime(time_str[0], time_fmt)
    except:
        try:
            time_fmt = "%m/%d/%y %H:%M"
            datetime.datetime.strptime(time_str[0], time_fmt)
        except:
            time_fmt = "%m/%d/%Y %H:%M"
            datetime.datetime.strptime(time_str[0], time_fmt)


    times_local = map(lambda x: datetime.datetime.strptime(x, time_fmt), time_str)

    site_data = [list(x) for x in site_data]

    for ndx, x in enumerate(site_data):

        site_data[ndx].append(np.array(times_local))
        site_data[ndx].append( np.array(data[ndx + 1]))

    return site_data

def mapToFloat(str_num):

    str_num = str_num.replace(",", "")
    if not str_num:
        return float('nan')
    try:
        num = float(str_num)
        return num
    except:
        pdb.set_trace()
        return float('nan')

def chunkSite(site_string):

   
    replacement = {"FREMONT   6022620601   OPP 600 CURTNER RD   P28604   E4775 (kW)": "FREMONT   6022620601   600 CURTNER RD   P28604   E4775 (kW)",
                   "FREMONT   6022620150   P29755   48485 AVALON HEIGHTS TER   E5225 (kW)": "FREMONT   6022620150   48485 AVALON HEIGHTS TER   P29755 E5225 (kW)",
                   "FREMONT   6022620445   42436 MISSION BLVD STE B   00398R   JJ932 (CGI-IN) (kW)": "FREMONT   6022620445   42436 MISSION BLVD STE B   00398R   JJ932 (kW)",
                   "OAKLAND   9803846005A   1950 FRANKLIN ST C27351   JQ3AB (kW)": "OAKLAND   98038460051   1950 FRANKLIN ST C27351   JQ3AB (kW)",
                   "OAKLAND   9803846005A   1950 FRANKLIN ST   C16275   JQ3AA (kW)": "OAKLAND   98038460051   1950 FRANKLIN ST C27351   JQ3AB (kW)",
                   "OAKLAND   9803846005B   1950 FRANKLIN ST C27351   JQ3AB (kW)": "OAKLAND   98038460052   1950 FRANKLIN ST C27351   JQ3AB (kW)",
                   "6720518005   WALNUT CREEK   25 VIA MONTE   C16277   JG691-1 (kW)": "WALNUT CREEK   6720518005 25 VIA MONTE   C16277   JG691-1 (kW)",
                   "VACAVILLE   0990900490   570 ORANGE DR   57P474   E5766-1 Co-Gen In (kWh+) (kW)": "VACAVILLE   0990900490   570 ORANGE DR   57P474   E5766-1 (kW)",
                   "VACAVILLE   0990900490   570 ORANGE DR   57P474   E5766-2 Co-Gen Out (kWh-) (kW)": "VACAVILLE   0990900490   570 ORANGE DR   57P474   E5766-1 (kW)",
                   "SUNNYVALE   1272589096   1347 CROSSMAN AVE   P28728   E5887-1 BUILDING DEMOLISHED 06-02-2008 (kW)": "SUNNYVALE   1272589096   1347 CROSSMAN AVE   P28728   E5887-1 (kW)"}


    if site_string in replacement.keys():
        site_string = replacement[site_string]

    tokens = site_string.split()
    for ndx, string in enumerate(tokens):
        try:
            site_id = int(string)
        except ValueError:
            continue

        break
    else:
        raise Exception("Could not chunk string '%s'" % site_string)

    city = " ".join(tokens[:ndx]).strip()
    address = " ".join(tokens[ndx + 1:-3]).strip()
    other = " ".join(tokens[-3:]).strip()
    
    return (site_id, address, city, other)

def addSiteToDatabase(conn, file_id, site_data):
    site_id, address, city, extra1, full_text, time_series, values, customer_name = site_data

    data_source = "PGE"
    end_use = ""
    square_footage = ""
    address_1 = address
    address_2 = ""
    state = "CA"
    zip_code = ""
    extra_info_1 = full_text
    extra_info_2 = extra1

    # FIND OUT IF THE SITE IS IN THE DATABASE; ADD IF NOT, GET ID IF YES

    site_db_id = TSD.getSiteID(conn, site_id)
    if site_db_id == -1:
        # ADD SITE TO DB AND SET SITE DB ID to result
        site_db_id = TSD.addNewSiteToDatabase(conn, site_id, customer_name, data_source, end_use, square_footage, \
                                                  address_1, address_2, city, state, zip_code, extra_info_1, extra_info_2)
    TSD.addNewSiteFileAssociation(conn, site_db_id, file_id)

    # RECORD THE SITE FILE MAPPING
    # ADD THE DATA 

    used_normalization, corrected_times = TSD.makeMonotonic(time_series)

    if not used_normalization: 
        timezone = "LOCAL"
    else:
        timezone = "NORMALIZED"
    
    TSD.addTimesToDatabase(conn, site_db_id, corrected_times, values, timezone, "kW")


    return 


def combineData(data):
    counts = {}
    indexes = {}
    for dat in data:
        counts[dat[0]] = 0
        indexes[dat[0]] = []

    for ndx, dat in enumerate(data):
        counts[dat[0]] += 1
        indexes[dat[0]].append(ndx)

    else:
        if max(counts.values()) == 1:
            return data

    new_data = []

    for site_id in counts.keys():
        if counts[site_id] == 1:
            new_data.append(data[indexes[site_id][0]])
        else:
            print "Combining site %s" % site_id
            indexes_to_combine = indexes[site_id]

            seed_index = indexes_to_combine.pop()

            datum = copy.deepcopy(data[seed_index])

            while len(indexes_to_combine):
                next_index = indexes_to_combine.pop()

                datum[6] = np.add(datum[6], data[next_index][6])

            new_data.append( datum)

    return new_data


def addFileToDatabase(cur, fn):
    file_id = TSD.addFileToDatabase(cur, fn, "PGE DATA")

    try:
        data = parsePGEFile(fn)

    except Exception, xcpt:
        print "Got exception.  Quitting..."
        print xcpt
        sys.exit(1)


    data = combineData(data)

    customer = os.path.split(fn)[1].split(".")[0]

    for site in data:
        site.append(customer)
        addSiteToDatabase(cur, file_id, site)
        
    return data

if __name__ == '__main__':
    main()
