import io
import copy
from IPython import embed
import MySQLdb as db
import os
import sys
import csv
import datetime
import numpy as np
import time
import pdb

def addFileToDatabase(conn, cur, dat_file):
    directory, filename = os.path.split(dat_file)
    data_source = "Phil provided it for the new Jessica Project; He originally got it from..."
    file_contents = open(dat_file).read()

    file_info = (filename, directory, data_source, file_contents)

    cur.execute("INSERT INTO files (filename, directory, data_source, file_contents) VALUES (%s, %s, %s, %s)", file_info)
    cur.connection.commit()

    cur.execute("show table status like 'files'")
    db_id = cur.fetchone()[10] - 1

    return db_id

def addNewFileMappingToDatabase(conn, cur, db_site_id, file_id):
    cur.execute("INSERT INTO file_sources (site_id, file_id) VALUES (%s, %s)", (db_site_id, file_id))
    cur.connection.commit()

def processFileAndAddToDatabase(fn, conn, cur):
    file_id = addFileToDatabase(conn, cur, fn)
    site_name = os.path.split(fn)[1].split("_")[0]
    new_site = (site_name, "Pulse")
    cur.execute("INSERT INTO sites (point_id, data_source) VALUES (%s, %s)", new_site)
    cur.connection.commit()

    cur.execute("show table status like 'sites'")
    site_id = cur.fetchone()[10] - 1

    addNewFileMappingToDatabase(conn, cur, site_id, file_id)


    with open(fn, "Ur") as in_file:
        file_reader = csv.reader(in_file, delimiter = ",")

        header_line = file_reader.next()
        try:
            assert(header_line[0].strip() == "Date")
            assert(header_line[1].strip() == "Electrical Demand" or header_line[1].strip() == "Electric Demand")
            assert(header_line[2].strip() == "Local OAT")
        except:
            pdb.set_trace()
            a = 10

        time_str_local = []
        kw_str = []
        oat_str = []

        for time_val, kw_val, oat_val in file_reader:
            time_str_local.append(time_val)
            kw_str.append(kw_val)
            oat_str.append(oat_val)

        else:
            fmt = "%d/%m/%Y %H:%M"
            time_dt_local = map( lambda x: datetime.datetime.strptime(x, fmt), time_str_local)
            
            def consFloat(x_str):
                if x_str == "NA" or x_str == "": 
                    return float("nan")
                else: 
                    try:
                        flt = float(x_str)
                        return flt
                    except:
                        pdb.set_trace()
                        a = 10
                        return float("nan")

    used_normalization, corrected_times = makeMonotonic(time_dt_local)
    kw_array = map(consFloat, kw_str)
    oat_array = map(consFloat, oat_str)

    if used_normalization:
        norm_string = "NORMALIZED"
    else:
        norm_string = "LOCAL"

    def kwElmtMaker(x):
        base_pair = [site_id]
        base_pair.extend(x[0])
        base_pair.append(x[1])
        base_pair.append(norm_string)
        base_pair.append("kW")
        base_pair.append(x[2])
        return base_pair




    # pdb.set_trace()
    kw_array = np.array(kw_array)
    oat_array = np.array(oat_array)

    kw_isnan = np.isnan(kw_array)
    oat_isnan = np.isnan(oat_array)

    for ndx, value in enumerate(kw_isnan):
        if value: kw_array[ndx] = 0.0

    for ndx, value in enumerate(oat_isnan):
        if value: oat_array[ndx] = 0.0

    kw_insertions = map(kwElmtMaker, zip(copy.deepcopy(corrected_times), kw_array, kw_isnan))

    cur.executemany("insert into trend_load (site_id, time, offset_sec, original_time, value, timezone, unit, is_nan) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", kw_insertions)
    cur.connection.commit()


    # ADD THE NEW RECORD OF THE WEATHER STREAM 
    # ADD THE NEW RECORD OF THE WEATHER STREAM
    cur.execute("INSERT INTO weather_data (official_stream, data_source) VALUES (%s, %s)", (True, "Included with original data set"))
    cur.connection.commit()
    cur.execute("show table status like 'weather_data'")
    weather_stream_id = cur.fetchone()[10] - 1

    def tempElmtMaker(x):
        base_pair = [weather_stream_id]
        base_pair.extend(x[0])
        base_pair.append(x[1])
        base_pair.append(norm_string)
        base_pair.append("C")
        base_pair.append(x[2])
        return base_pair

    oat_insertions = map(tempElmtMaker, zip(copy.deepcopy(corrected_times), oat_array, oat_isnan))
    cur.executemany("insert into trend_temp (weather_id, time, offset_sec, original_time, value, timezone, unit, is_nan) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", oat_insertions)
    cur.connection.commit()

    # ADD THE RECORD SAYING THE SITE CAN BE USED WITH THE WEATHER
    # ADD A RECORD SAYING THAT THE SITE CAN BE USED WITH THE WEATHER.
    cur.execute("INSERT INTO sites_weather (site_id, weather_id) VALUES (%s, %s)", (site_id, weather_stream_id))
    cur.connection.commit()

    

    # pdb.set_trace()

    # cur.executemany("insert into trend_load (site_id, time, offset_sec, original_time, value, timezone, unit) VALUES (%s, %s, %s, %s, %s, %s, %s)", kw_insertions)

    # for dat in kw_insertions:
    #     try:
    #         #cur.execute("insert into trend_load (site_id, time, offset_sec, original_time, value, timezone, unit, is_nan) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", dat)
    #         cur.connection.commit()
    #     except Exception, xcpt:
    #         pdb.set_trace()
    #         a = 10

    

    return 

def isMonotonic(time_array):
    spacings = []

    for ndx in range(len(time_array) - 1):
        spacings.append( (time_array[ndx+1] - time_array[ndx]).total_seconds())
    
    unique_spacings = set(spacings)
    for sp in unique_spacings:
        print "%s: %d" % (sp / 60., spacings.count(sp))

    return 

def processLocalTimeStrip(time_dt_local):
    time_dt_local = np.array(time_dt_local)

    # embed()
    isMonotonic(time_dt_local)
        
    return False, False


def makeMonotonic(ts):
    ts = copy.copy(ts)

    first_point = ts.pop(0)

    times = [ [first_point, 0.0, first_point] ]

    used_normalization = False

    for time_point in ts:
        # Is monontonic
        if time_point > times[-1][0]:

            times.append([time_point, 0.0, time_point])
        else:
            used_normalization = True
            spacing = (time_point - times[-1][0]).total_seconds()
            assert(spacing <= 0.0)

            correction = abs(spacing) + 1.0
            adjusted_time = time_point + datetime.time_delta(seconds = correction)

            times.append([adjusted_time, -correction, time_point])

    return used_normalization, times

def checkMonotonic(ts):
    for ndx in range(len(ts) - 1):
        if ts[ndx] >= ts[ndx + 1]:
            return False
        else:
            return True

def processFileAndCheckTimeSeries(fn):
    # file_id = addFileToDatabase(conn, cur, fn)

    with open(fn, "Ur") as in_file:
        file_reader = csv.reader(in_file, delimiter = ",")

        header_line = file_reader.next()
        assert(header_line[0].strip() == "Date")
        assert(header_line[1].strip() == "Electrical Demand" or header_line[1].strip() == "Electric Demand")
        assert(header_line[2].strip() == "Local OAT")

        time_str_local = []

        for time_val, kw_val, oat_val in file_reader:
            time_str_local.append(time_val)

        else:
            fmt = "%d/%m/%Y %H:%M"
            time_dt_local = map( lambda x: datetime.datetime.strptime(x, fmt), time_str_local)

        if not checkMonotonic(time_dt_local):
            print "%s has a fall back." % fn

        corrected_times = makeMonotonic(time_dt_local)

        # Now we can insert these

    return 



if __name__ == '__main__':

    data_dir = "PulseData"
    
    files = map(lambda x: os.path.join(data_dir, x), os.listdir(data_dir))

    conn = db.connect("localhost", "maronnax", "f112358S", "building_db")
    cur = conn.cursor()

    for dat_file in files:

        print "Processing %s" % dat_file
        processFileAndAddToDatabase(dat_file, conn, cur)
        # processFileAndCheckTimeSeries(dat_file)
