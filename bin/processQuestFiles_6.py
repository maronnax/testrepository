import io
import MySQLdb as db
import os
import sys
import copy
import csv
import datetime
import numpy as np
import time
import pdb
import TSD
import QuestTables

def makeFilePairs(building_info_files, data_files):
    pairs = {}

    for fn in building_info_files:
        prekey = fn.split("_")[1]
        num = int(prekey[:-4])
        pairs.setdefault(num, []).append(fn)

    for fn in data_files:
        num = int(fn.split("_")[1])
        pairs.setdefault(num, []).append(fn)

    for key in pairs.keys():
        assert(len(pairs[key]) == 2)
    
    return pairs

def main():

    data_dir = "../QuestData_6"
    conn = TSD.connect()
    cur = conn.cursor()

    building_info_files = filter(lambda x: x.startswith("buildinginfo_"), os.listdir(data_dir))
    data_files = filter(lambda x: x.startswith("data_"), os.listdir(data_dir))

    building_data_pairs = makeFilePairs(building_info_files, data_files)

    for key in building_data_pairs:
        building_data_pairs[key] = map(lambda x: os.path.join(data_dir, x), building_data_pairs[key])
    
    for quest_site_id in building_data_pairs.keys():
        files = building_data_pairs[quest_site_id]
        files.sort()

        site_id = addFileToDatabase(cur, files[0], files[1])

def processServiceAddress(service_addr):
    addr_comps = service_addr.split(",")
    if len(addr_comps) == 5:
        return addr_comps
    else:
        addr_1, city, state, zip_code = addr_comps
        return (addr_1, "", city, state, zip_code)

    return

def collapseInteractive(list_of_options):
    
    if len(set(list_of_options)) == 1:
        return list_of_options[0]
    else:
        filt_1 = filter(lambda x: "No Building Type Available" not in x, list_of_options)
        filt_2 = filter(lambda x: "OTR" not in x, filt_1)
        if len(set(filt_2)) == 1:
            return filt_2[0]

        
        while True:
            opt_dict = dict(enumerate(set(list_of_options)))
            for x,y in opt_dict.items():
                print "%d:\t%s" % (x,y)

            try:
                inp = int(raw_input("Choice: "))
                return opt_dict[inp]
            except KeyError:
                continue
            except ValueError:
                continue
    return

sites = []

def addMetadataToDatabase(cur, fn, headers, site_info):

    def selectNdxs(arr, ndxs):
        return [arr[x] for x in ndxs]
    # 2: DO_NOT_USE_THIS_ID -> 
    pass

    # 4: SA_ID -> site:point_id
    # 1: ZIPCODE -> sites:zip_code
    # 11: ENTITY_NAME -> sites:customername
    # 15: SERVICE_ADDR -> PARSE into sites:address_1, sites:address_2, sites:city, sites:state

#    pdb.set_trace()
    site_table_ndxs = [4, 1, 11, 15]
    site_values = map(lambda arr: selectNdxs(arr,site_table_ndxs), site_info)
    list_choices = map(lambda n: [s[n] for s in site_values], range(len(site_table_ndxs)))
    values = map(collapseInteractive, list_choices)
    sa_id, zipcode, entity_name, service_addr = values
    address_1, address_2, city, state, zipcodewithjunk = processServiceAddress(service_addr)

    assert(zipcodewithjunk.startswith(zipcode))

#    print "Inserting( %s, %s, %s, %s, %s, %s, %s)" % (sa_id, entity_name, address_1, address_2, city, state, zipcode)

    print "Installing site %s" % sa_id
    new_site_id = TSD.addNewSiteToDatabase(cur, sa_id, customer_name = entity_name, data_source = "QuEST Project,Single Channel", \
                                           address_1 = address_1, address_2 = address_2, city = city, state = state, \
                                           zip_code = zipcode)

    # 0: QUEST_ID -> pge_metadata:quest_id
    # 3: ACCT_ID -> pge_metadata:acct_id
    # 8: PREM_ID -> pge_metadata:prem_id
    # 9: PER_ID -> pge_metadata:per_id
    # 10: MAILING_ADDR -> pge_metadata.mailing_addr
    # 12: NAICS2_CODE -> pge_metadata:naics2_code
    # 13: COMB_SIZE -> pge_metadata: comb_size
    # 14: COMB_SIZE_CD -> pge_metadata:comb_size_cd
    # 16: METER_TYPE -> pge_metadata:meter_type
    # 17: BLDG_TYPE_CODE -> pge_metadata:building_type_code
    # 18: DESCRIPTION -> pge_metadata:description
    # 19: CLIMATE_ZONE_CODE -> pge_metadata:climate_zone_code

    pge_metadata_ndxs = [0, 3, 8, 9, 10, 12, 13, 14, 16, 17, 18, 19]
    pge_metadata_values = map(lambda arr: selectNdxs(arr, pge_metadata_ndxs), site_info)
    list_choices = map(lambda n: [s[n] for s in pge_metadata_values], range(len(pge_metadata_ndxs)))
    values = map(collapseInteractive, list_choices)
    quest_id, acct_id, prem_id, per_id, mailing_addr, naics2_code, comb_size, comb_size_cd, \
              meter_type, bldg_type_code, description, climate_zone_code = values

    pge_metadata_id = QuestTables.addNewPGESiteMetadata(cur, new_site_id, quest_id = quest_id, acct_id = acct_id, prem_id = prem_id, \
                                                        per_id = per_id, mailing_addr = mailing_addr, naics2_code = naics2_code, \
                                                        comb_size = comb_size, comb_size_cd = comb_size_cd, meter_type = meter_type, \
                                                        bldg_type_code = bldg_type_code, description = description, \
                                                        climate_zone_code = climate_zone_code)

    # print "Inserting |%s|" % ("|".join(values))

    # 5: MEASURE_CODE -> pge_retrofits:measure_code
    # 6: MEASURE_DESC -> pge_retrofits:measure_desc
    # 7: INSTALL_DATE -> pge_retrofits:install_date
    # 26: INSTALL_DATE_F -> ** Check that it matches #7, but otherwise throw away.
    # 20: PGE_PROGRAM_NAME -> pge_retrofits:pge_program_name
    # 21: PGE_PROG_SUBCAT -> pge_retrofits:pge_prog_subcat
    # 22: CHANNEL -> pge_retrofits:channel
    # 23: DELIVERY_TYPE -> pge_retrofits:delivery_type
    # 24: DELIVERY_TYPE_DESC -> pge_retrofits:delivery_type_desc
    # 25: RS_CD -> pge_metadata:rs_cd
    retrofit_ndxs = [5, 6, 7, 26, 20, 21, 22, 23, 24, 25]
    retrofit_values = map(lambda arr: selectNdxs(arr, retrofit_ndxs), site_info)

    for retro in retrofit_values:
        measure_code, measure_desc, install_date, install_date_f, pge_prog_name, \
                      pge_prog_subcat, channel, delivery_type, delivery_type_desc, rs_cd = retro


        # print "|%s|%s|" % (sa_id, "|".join(retro))

        if install_date.strip() == "":
            install_date_string = ""
        else:
            month, day, year = map(int, install_date.split("/"))
            install_dt = datetime.date(month = month, year = year, day = day)
            
            install_date_string = "%.4d-%.2d-%.2d" % (install_dt.year, install_dt.month, install_dt.day)
            
        
        retrofit_id = QuestTables.addPGERetrofit(cur, new_site_id, install_date_string, measure_code, measure_desc, 
                                                 delivery_type, delivery_type_desc, 
                                                 pge_prog_name, pge_prog_subcat, 
                                                 channel, rs_cd)
    return new_site_id

def parseDataFileName(cur, site_id, data_file):

    ts = []
    vs = []
    data_type = data_file.split("_")[-1][:-4]
    
    with open(data_file, 'Ur') as in_file:
        data_reader = csv.reader(in_file, delimiter=",")
        data_reader.next() # Kill the header

        for time_str, value in data_reader:
            ts.append( datetime.datetime.strptime( time_str, "%Y-%m-%d %H:%M:%S"))
            vs.append(float(value))

    used_normalization, corrected_times = TSD.makeMonotonic(ts)

    if not used_normalization: 
        timezone = "LOCAL"
    else:
        timezone = "NORMALIZED"

    if data_type.lower() == "kw": data_type = "kW"
    TSD.addLoadDataToDatabase(cur, site_id, corrected_times, vs, timezone, "kW")

    return 


def addFileToDatabase(db_cur, metadata_fn, data_fn):
    print "Processing %s" % metadata_fn

    headers, site_info = parseMetaDataFile(metadata_fn)

    new_site_id = addMetadataToDatabase(db_cur, metadata_fn, headers, site_info)
    parseDataFileName(db_cur,new_site_id, data_fn)

    return 

def parseMetaDataFile(fn):
    header = []
    lines = []
    with open(fn, 'Ur') as in_file:
        file_reader = csv.reader(in_file, delimiter=",")
        header = file_reader.next()
        for x in file_reader:
            lines.append(x)
    return header, lines

if __name__ == '__main__':
    main()
            
