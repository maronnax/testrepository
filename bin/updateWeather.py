def getAllRawWeatherForZip(cur, zip_code):
    num = cur.execute("SELECT point_id FROM weather_stations WHERE zip_code='%s' AND source_description='Raw'" % zip_code)
    return map(lambda x: x[0], cur.fetchall())

def getProcessedWeather(cur):
    num = cur.execute("SELECT point_id FROM weather_stations WHERE zip_code='%s' AND source_decription='Processed'" % zip_code)
    return map(lambda x: x[0], cur.fetchall())

def dropProcessedWeather(cur):
    num_dropped = cur.execute("DROP FROM weather_stations WHERE source_decription='Processed'")
    return num_dropped

def getAllWeatherZips(cur):
    num = cur.execute("SELECT DISTINCT zip_code from weather_stations")
    return map(lambda x: x[0], cur.fetchall())

def createProcessedZipCodeWeatherData(cur, zip_code):
    num_sites = cur.execute("SELECT * FROM sites WHERE zip_code='%s'" % zip_code)
    # Load up all of them

    # This needs viz data.
    
    # Throw out bad ones
    # Resample
    # Take the median
    # Re-add it
    return 

def processWeather():
    zip_codes = getAllWeatherZips()
    for zip_code in zip_codes:
        createProcessedZipCodeWeatherData(zip_code)
    return 
