""" This module contains Quest extentions to the standard
TimeSeriesDatabase (TSD) interface.

This includes functions for adding/selecting data from the quest
specific tables.

"""

import TSD


# Extra PGE Tables
# -----------------
# pge_retrofits
# pge_metadata

def addNewPGESiteMetadata(cur, site_id, quest_id = "", acct_id = "", prem_id = "", \
                          per_id = "", mailing_addr = "", naics2_code = "", comb_size = "", \
                          comb_size_cd = "", meter_type = "", bldg_type_code = "", description = "", \
                          climate_zone_code = "", rs_cd = ""):
    """
    """
    
    cur.execute("INSERT INTO pge_metadata (site_id, acct_id, quest_id, prem_id, per_id, mailing_addr, naics2_code, comb_size, comb_size_cd, bldg_type_code, description, meter_type, climate_zone_code, rs_cd) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (site_id, acct_id, quest_id, prem_id, per_id, mailing_addr, naics2_code, comb_size, comb_size_cd, \
                 bldg_type_code, description, meter_type, climate_zone_code, rs_cd))
    cur.connection.commit()
    return TSD.getLastInsertionID(cur, "pge_metadata")

def addPGERetrofit(cur, site_id, install_date="", measure_code="", measure_desc="", 
                   delivery_type="", delivery_type_desc="", 
                   pge_prog_name="", pge_prog_subcat="", channel="", rs_cd=""):

    print "Adding retrofit - '%s'" % install_date
    cur.execute( "INSERT INTO pge_retrofits (site_id, install_date, measure_code, measure_description, delivery_type, delivery_type_desc, pge_prog_name, pge_prog_subcat, channel, rs_cd) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", \
                     (site_id, install_date, measure_code, measure_desc, delivery_type, delivery_type_desc, 
                      pge_prog_name, pge_prog_subcat, channel, rs_cd))
    cur.connection.commit()
    return TSD.getLastInsertionID(cur, "pge_retrofits")

def getNumberOfRetrofits(cur, site_id):
    num = cur.execute("SELECT COUNT(*) FROM pge_retrofits").fetchone()[0]
    return num

def checkNumberUndatedRetrofits(cur, site_id):
    num = cur.execute("SELECT COUNT(*) FROM pge_retrofits where site_id='%s' and install_date='';" %site_id)
    return num

def getDateOfFirstRetrofit(cur, site_id):
    num = cur.execute("SELECT install_date from pge_retrofits where site_id='%s' ORDER BY install_date LIMIT 1;")

    if not num:
        return False
    else:
        return cur.fetchone()[0]
    return

def getDataForSiteBetweenRetrofits(cur, site_id, retro_a, retro_b):
    num = cur.execute("SELECT install_date from pge_retrofits where site_id='%s' ORDER BY install_date;")
    if num == 0:
        return
    elif num == 1:
        return

    retro_dates = cur.fetchall()

    date_1 = retro_dates[retro_a]
    date_2 = retro_dates[retro_b]

    ts, vs = TimeSeriesDatabase.getLoadDataForSite(cur, site_id, \
                                                   lower_bound = date_1, \
                                                   upper_bound = date_2)
    return ts, vs
    
