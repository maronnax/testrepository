import MySQLdb as db
import pdb
import os
import datetime
import numpy as np
import datetime
import copy

"""This module provides an api to the standard tables in the
TimeSeriesDatabase (TSD), both for adding and for querying.

The standard tables are:
-------------------------
files
sites
file_sources
weather_data
sites_weather
trend_load
trend_temp

"""

def connect():
    conn = db.connect("localhost", "maronnax", "alphabravo9er", "building_db")
    return conn

def getLastInsertionID(cur, table_name):
    cur.execute("show table status like '%s'" % table_name)
    last_id = cur.fetchone()[10] - 1
    return last_id

# Files table 

# For now, we only have one function that adds a file to the database
# and do not have the corresponding file to download the file FROM the
# database.
def addFileToDatabase(cur, filename, data_source = ""):
    """Add a file to the database, by supplying the filename, and an
    optional source of the file."""
    directory, filename = os.path.split(filename)
    file_contents = open(filename).read()
    file_info = (filename, directory, data_source, file_contents)
    cur.execute("INSERT INTO files (filename, directory, data_source, file_contents) VALUES (%s, %s, %s, %s)", file_info)
    cur.connection.commit()
    return getLastInsertionID(cur, "files")

# Sites table
def addNewSiteToDatabase(cur, site_id, customer_name="", data_source="", end_use="", square_footage="", \
                             address_1="", address_2="", city="", state="", zip_code="", extra_info_1="", extra_info_2=""):
    cur.execute("INSERT INTO sites (point_id, customer_name, data_source, end_use, square_footage, address_1, address_2, city, state, zip_code, extra_info, extra_info_2) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", \
                    (site_id, customer_name, data_source, end_use, square_footage, address_1, address_2, city, state, zip_code, extra_info_1, extra_info_2))
    cur.connection.commit()
    return getLastInsertionID(cur, "sites")

# Add to file_sources table.
def addNewSiteFileAssociation(conn, site_id, file_id):
    conn.execute("INSERT INTO file_sources (site_id, file_id) VALUES (%s, %s)", (site_id, file_id))
    conn.connection.commit()
    return 

def makeMonotonic(ts):
    """This function takes a time series array, assumed to be
    monotonically increasing, and returns a boolean indicating whether
    the time list was already in normalized form, and a list of
    triplets in the form (normalized_time, offset, original_time)."""

    ts = list(ts) 
    ts = copy.copy(ts)
    first_point = ts.pop(0)
    times = [ [first_point, 0.0, first_point] ]
    used_normalization = False

    for time_point in ts:
        # Is monontonic
        if time_point > times[-1][0]:

            times.append([time_point, 0.0, time_point])
        else:
            used_normalization = True
            spacing = (time_point - times[-1][0]).total_seconds()
            assert(spacing <= 0.0)

            correction = abs(spacing) + 1.0
            adjusted_time = time_point + datetime.timedelta(seconds = correction)

            times.append([adjusted_time, -correction, time_point])

    return used_normalization, np.array(times)

# Add to the trend_load table
def addLoadDataToDatabase(cur, site_id, time_list, values, timezone, unit):
    """This takes a site_id, and lists of the times to add, times
    should be in the form (norm_time, offset, original_time), as produced by the """

    assert(len(time_list) == len(values))
    insertions = []

    isnan_list = ["False" for x in values]
    for ndx, x in enumerate(values):
        if np.isnan(x):
            isnan_list[ndx] = "True"
            values[ndx] = 0.0
    for ndx in xrange(len(values)):
        ct, off, ot = time_list[ndx]
        value = values[ndx]
        isnan = isnan_list[ndx]
        insertions.append( (site_id, ct, off, ot, value, isnan, timezone, unit) )

    cur.executemany( "INSERT INTO trend_load (site_id, time, offset_sec, original_time, value, is_nan, timezone, unit) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", insertions)
    cur.connection.commit()

    return 


# TODO
# Write stuff for weather_stations
# Write stuff for sites_to_stations
# Write stuff for trend_temp

def addWeatherSiteToDatabase(cur, point_id, station_type = "", source_description="", address_1="", \
                                 address_2="", city="", state="", zip_code=""):
    val = (point_id, station_type, source_description, address_1, address_2, city, state, zip_code)
    cur.execute("INSERT INTO weather_stations (point_id, station_type, source_description, address_1, address_2, city, state, zip_code) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", val)
    cur.connection.commit()
    return getLastInsertionID(cur, "weather_stations")

def addSitesToStationsMapping(cur, site_id, station_id, is_official=False, distance = 1e10, description = ""):
    val = (site_id, station_id, is_official, distance, description)
    cur.execute("INSERT INTO sites_to_stations (site_id, station_id, is_official_source, distance, description) VALUES (%s,%s,%s,%s,%s)", val)
    cur.connection.commit()
    return 

# Add to the trend_load table
def addTemperatureDataToDatabase(cur, station_id, time_list, values, timezone, unit):
    """This takes a site_id, and lists of the times to add, times
    should be in the form (norm_time, offset, original_time), as produced by the """

    assert(len(time_list) == len(values))
    insertions = []

    isnan_list = ["False" for x in values]
    for ndx, x in enumerate(values):
        if np.isnan(x):
            isnan_list[ndx] = "True"
            values[ndx] = 0.0
    for ndx in xrange(len(values)):
        ct, off, ot = time_list[ndx]
        value = values[ndx]
        isnan = isnan_list[ndx]
        insertions.append( (station_id, ct, off, ot, value, isnan, timezone, unit) )

    cur.executemany( "INSERT INTO trend_temperature (site_id, time, offset_sec, original_time, value, is_nan, timezone, unit) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", insertions)
    cur.connection.commit()

    return 

def lookupSiteIDFromPointID(cur, site_id):
    if cur.execute("select id from sites where point_id=%s", site_id):
        return cur.fetchone()[0]
    else:
        return -1

